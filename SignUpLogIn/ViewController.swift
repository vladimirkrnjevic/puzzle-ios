//
//  ViewController.swift
//  SignUpLogIn
//
//  Created by Vladimir on 2/27/18.
//  Copyright © 2018 vladimir.krnjevic. All rights reserved.
//

import UIKit
import FirebaseAuth


//, FBSDKLoginButtonDelegate
class ViewController: UIViewController {
    
    @IBOutlet weak var signInSelector: UISegmentedControl!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    var isSignIn = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        

        
    }
    
    @IBAction func signInSelectorChanged(_ sender: Any) {
        //Flip the boolean
        isSignIn = !isSignIn
        
        //Check the bool and set the button title and labels
        if isSignIn {
            signInButton.setTitle("Sign In", for: .normal)
        } else {
            signInButton.setTitle("Register", for: .normal)
            }
    }
        
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        
        if let email = emailTextField.text, let pass = passwordTextField.text {
            
            // Check if it's sign in or register
            if isSignIn {
                // Sign in the user with Firebase
                Auth.auth().signIn(withEmail: email, password: pass, completion: { (user, error) in
                    // Check that user isn't nil
                    if let u = user {
                        // User is found go to home screen
                        self.performSegue(withIdentifier: "goToHome", sender: self)
                    } else {
                        // Error: check error and show message
                    }
                })
            } else {
                // Register the user with Firebase
                Auth.auth().createUser(withEmail: email, password: pass, completion: { ( user,error) in
                    // Check that user isn't nil
                    if let u = user {
                        // User is found go to home screen
                        self.performSegue(withIdentifier: "goToHome", sender: self)
                    } else {
                        // Error: check error and show message
                    }
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Dismiss the keyboard when the view is tapped on
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    
    
}
    
    
    

    
    
    


